titan-agent
=============

Titan-agent - An agent runs inside VM, gather information for titan server.

http://www.titan-engine.net

http://peter.kingofcoders.com

How to use:

1. Download the latest jar from http://download.titan-engine.net
2. java -jar titan-agent.jar , you will see this message "main.properties created, please edit it and restart"
3. edit main.properties, replace the instanceId to your VM's instance Id. This ID can be obtained in titan's vm page
4. java -jar titan-agent.jar <-- execute this line again, the agent will be running
