import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

public class TestVideoEncoder {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_hhmmss");

	public static void main(String args[]) {
		new TestVideoEncoder();
	}

	public TestVideoEncoder() {
		screenCapture();
		generateVideo("video/a.mp4", new File("video").list());
	}

	private void screenCapture() {
		new Thread() {
			public void run() {
				while (true) {
					try {
						BufferedImage screencapture = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

						File file = new File("video/" + sdf.format(new Date()) + ".jpg");
						ImageIO.write(screencapture, "jpg", file);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		}.start();
	}

	private static void generateVideo(String file, String jpegFiles[]) {
		//		try {
		//			final long duration = DEFAULT_TIME_UNIT.convert(1, SECONDS);
		//			final int videoStreamIndex = 0;
		//			final int videoStreamId = 0;
		//			final long frameRate = DEFAULT_TIME_UNIT.convert(5, MILLISECONDS);
		//			final int width = 320;
		//			final int height = 200;
		//			long nextFrameTime = 0;
		//			final IMediaWriter writer = ToolFactory.makeWriter(file);
		//			writer.addVideoStream(videoStreamIndex, videoStreamId, ICodec.ID.CODEC_ID_H264, width, height);
		//			while (nextFrameTime < duration) {
		//				System.out.println("time: " + nextFrameTime + " , duration: " + duration);
		//				for (int x = 0; x < jpegFiles.length; x++) {
		//					System.out.println("<" + jpegFiles[x]);
		//					BufferedImage img1 = ImageIO.read(new File(jpegFiles[x]));
		//					BufferedImage frame = new BufferedImage(320, 200, BufferedImage.TYPE_3BYTE_BGR);
		//					Graphics2D gr = (Graphics2D) frame.getGraphics();
		//					gr.drawImage(img1, null, 0, 0);
		//					writer.encodeVideo(videoStreamIndex, frame, nextFrameTime, DEFAULT_TIME_UNIT);
		//					nextFrameTime += frameRate;
		//				}
		//			}
		//			writer.close();
		//			System.out.println("end generateVideo");
		//		} catch (Exception ex) {
		//			ex.printStackTrace();
		//		}
	}
}
