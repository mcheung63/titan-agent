package com.titanagent;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.CheckboxMenuItem;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.titanagent.quartz.GatherJob;

public class TitanAgent extends JFrame {
	private static Logger logger = Logger.getLogger(TitanAgent.class);

	public enum OSType {
		mac, win, linux
	};

	private JPanel contentPane;
	public static OSType os;
	private static CommandLine cmd;

	public static void main(String[] args) {
		CommandLineParser parser = new PosixParser();
		Options options = new Options();
		try {
			options.addOption("v", "version", false, "display version info");
			options.addOption("s", "setup", false, "first time setup");
			cmd = parser.parse(options, args);
		} catch (ParseException e1) {
			e1.printStackTrace();
			System.exit(0);
		}
		if (cmd.hasOption("version") || cmd.hasOption("v")) {
			System.out.println("version : " + Global.version);
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("run : java -jar titan-agent.jar [OPTION]", options);
			System.exit(0);
		}
		if (cmd.hasOption("setup") || cmd.hasOption("s")) {
			try {
				IOUtils.copy(TitanAgent.class.getResourceAsStream("/main.properties"), new FileOutputStream("main.properties"));
				System.out.println("main.properties created");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			System.exit(0);
		}
		if (!new File("main.properties").exists()) {
			try {
				IOUtils.copy(TitanAgent.class.getResourceAsStream("/main.properties"), new FileOutputStream("main.properties"));
				System.out.println("main.properties created, please edit it and restart");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			System.exit(0);
		}

		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.toLowerCase().contains("windows")) {
			os = OSType.win;
		} else if (osName.toLowerCase().contains("mac")) {
			os = OSType.mac;
		} else {
			os = OSType.linux;
		}

		if (SystemTray.isSupported()) {
			Image trayImage = new ImageIcon(TitanAgent.class.getClassLoader().getResource("com/titanagent/titanTrayIcon.png")).getImage();
			if (osName.toLowerCase().contains("windows")) {
				BufferedImage resizedImage = TitanAgentCommonLib.resizeImage(TitanAgentCommonLib.toBufferedImage(trayImage), 14, 14, BufferedImage.TYPE_INT_ARGB);
				trayImage = TitanAgentCommonLib.bufferedImagetoImage(resizedImage);
			}
			TrayIcon trayIcon = new TrayIcon(trayImage);
			try {
				PopupMenu popup = new PopupMenu();
				// Create a pop-up menu components
				MenuItem aboutItem = new MenuItem("About");
				CheckboxMenuItem cb1 = new CheckboxMenuItem("Set auto size");
				CheckboxMenuItem cb2 = new CheckboxMenuItem("Set tooltip");
				Menu displayMenu = new Menu("Display");
				MenuItem errorItem = new MenuItem("Error");
				MenuItem warningItem = new MenuItem("Warning");
				MenuItem infoItem = new MenuItem("Info");
				MenuItem noneItem = new MenuItem("None");
				MenuItem exitItem = new MenuItem("Exit");

				//Add components to pop-up menu
				popup.add(aboutItem);
				popup.addSeparator();
				popup.add(cb1);
				popup.add(cb2);
				popup.addSeparator();
				popup.add(displayMenu);
				displayMenu.add(errorItem);
				displayMenu.add(warningItem);
				displayMenu.add(infoItem);
				displayMenu.add(noneItem);
				popup.add(exitItem);

				trayIcon.setPopupMenu(popup);

				SystemTray tray = SystemTray.getSystemTray();
				tray.add(trayIcon);
			} catch (AWTException e) {
				System.out.println("TrayIcon could not be added.");
			}
		}

		//		if (os == OSType.win || os == OSType.mac) {
		//			EventQueue.invokeLater(new Runnable() {
		//				public void run() {
		//					try {
		//						final TitanAgent frame = new TitanAgent();
		//						frame.setVisible(true);
		//					} catch (Exception e) {
		//						e.printStackTrace();
		//					}
		//				}
		//			});
		//		}
		startQuartz();
	}

	public TitanAgent() {
		setTitle("Titan agent");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

	private static void startQuartz() {
		try {
			Properties prop = new Properties();
			FileInputStream inputStream = new FileInputStream(new File("main.properties"));
			prop.load(inputStream);

			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();

			logger.info(scheduler.getSchedulerName() + " started");

			JobDetail job = JobBuilder.newJob(GatherJob.class).withIdentity("Titan Agent Job").build();
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1").startNow()
					.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(Integer.parseInt(prop.getProperty("quartz_interval"))).repeatForever()).build();
			scheduler.scheduleJob(job, trigger);
		} catch (Exception se) {
			se.printStackTrace();
		}
	}

}
