package com.titanagent;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class TitanAgentCommonLib {
	private static Logger logger = Logger.getLogger(TitanAgentCommonLib.class);

	public static String filterJSonSpecialCharacter(String s) {
		if (s == null) {
			return "";
		} else {
			s = s.replaceAll("\n", "\\n");
			return s.replaceAll("\\\\", "\\\\\\\\").replaceAll("\"", "\\\\\"");
		}
	}

	private byte[] toByteArray(Blob fromBlob) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			return toByteArrayImpl(fromBlob, baos);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException ex) {
				}
			}
		}
	}

	private byte[] toByteArrayImpl(Blob fromBlob, ByteArrayOutputStream baos) throws SQLException, IOException {
		byte[] buf = new byte[4000];
		InputStream is = fromBlob.getBinaryStream();
		try {
			for (;;) {
				int dataSize = is.read(buf);
				if (dataSize == -1)
					break;
				baos.write(buf, 0, dataSize);
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException ex) {
				}
			}
		}
		return baos.toByteArray();
	}

	public static String sendPost(String url, Hashtable ht) throws Exception {
		URL urlObj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();

		con.setRequestMethod("POST");
		String urlParameters = "";

		Enumeration ee = ht.keys();
		String key = null;
		while (ee.hasMoreElements()) {
			key = (String) ee.nextElement();
			Object obj = ht.get(key);
			urlParameters += key + "=" + obj + "&";
		}

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		//                String inputLine;
		//                StringBuffer response = new StringBuffer();
		//
		//                while ((inputLine = in.readLine()) != null) {
		//                        response.append(inputLine);
		//                }
		//                in.close();

		return IOUtils.toString(in);
	}

	public static String sendPost(String url, Hashtable ht, File file) throws Exception {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost post = new HttpPost(url);

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		post.setEntity(new UrlEncodedFormEntity(nvps, Consts.UTF_8));

		MultipartEntity entity = new MultipartEntity();
		if (file != null) {
			entity.addPart("image", new FileBody(file));
		}
		Enumeration<String> it = ht.keys();
		while (it.hasMoreElements()) {
			String key = it.nextElement();
			String value = (String) ht.get(key);
			entity.addPart(key, new StringBody(value, Charset.forName("UTF-8")));
		}
		post.setEntity(entity);

		HttpResponse response = client.execute(post);
		HttpEntity entityResponse = response.getEntity();
		String r = EntityUtils.toString(entityResponse);
		client.close();
		return r;
	}

	public static BufferedImage resizeImage(BufferedImage originalImage, int width, int height, int type) {
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();

		return resizedImage;
	}

	public static BufferedImage toBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();

		// Return the buffered image
		return bimage;
	}

	public static Image bufferedImagetoImage(BufferedImage bi) {
		return Toolkit.getDefaultToolkit().createImage(bi.getSource());
	}
}
