package com.titanagent.quartz;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.hyperic.sigar.ProcCpu;
import org.hyperic.sigar.ProcMem;
import org.hyperic.sigar.ProcState;
import org.hyperic.sigar.Sigar;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.titanagent.TitanAgentCommonLib;

@DisallowConcurrentExecution
public class GatherJob implements Job {
	private String url = null;
	private String instanceId = null;
	private Sigar sigar;
	static int count = 1;
	SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	boolean sendToServer = true;
	String version = "1";

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			if (url == null || instanceId == null) {
				Properties prop = new Properties();
				FileInputStream inputStream = new FileInputStream(new File("main.properties"));
				prop.load(inputStream);
				url = prop.getProperty("titanRestServer");
				instanceId = prop.getProperty("instanceId");

			}
			JSONObject json = new JSONObject();
			sigar = new Sigar();
			//			final Vector<JSONObject> v = new Vector<JSONObject>();
			int noOfthread = sigar.getProcList().length / 4;
			//			if (noOfthread > 40) {
			//				noOfthread = 40;
			//			}
			ExecutorService tp = Executors.newFixedThreadPool(noOfthread);

			final JSONObject processes = new JSONObject();
			for (long pid : sigar.getProcList()) {
				final long pidFinal = pid;
				tp.execute(new Runnable() {
					public void run() {
						try {
							// need to create a new sigar per thread, otherwise core dump
							Sigar sigar2 = new Sigar();
							String procArgs[] = sigar2.getProcArgs(pidFinal);
							ProcState procState = sigar2.getProcState(pidFinal);
							ProcMem procMem = sigar2.getProcMem(pidFinal);
							String commandLine = StringUtils.join(procArgs, ", ");

							ProcCpu procCpu = sigar2.getProcCpu(pidFinal);
							procCpu.gather(sigar2, pidFinal);
							Thread.sleep(2000);
							ProcCpu procCpu2 = sigar2.getProcCpu(pidFinal);

							HashMap<String, Object> m = new HashMap<String, Object>();
							m.put("pid", pidFinal);
							m.put("procState", procState);
							m.put("procCpu", procCpu2);
							m.put("procMem", procMem);
							m.put("commandLine", commandLine);

							System.out.println(commandLine + "," + pidFinal + ", " + procState);

							processes.put(String.valueOf(pidFinal), m);
						} catch (Exception ex) {
						}
					}
				});
			}

			tp.shutdown();
			tp.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
			JSONObject result = new JSONObject();
			result.put("processes", processes);
			json.put("result", result);
			//			System.out.println(formatJSon(json.toString()));

			if (sendToServer) {
				Hashtable<String, String> ht = new Hashtable<String, String>();
				ht.put("version", version);
				ht.put("instanceId", instanceId);
				ht.put("category", "process");
				ht.put("message", json.toString());
				File temp = null;
				try {
					BufferedImage screencapture = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
					temp = File.createTempFile("img", ".jpg");
					ImageIO.write(screencapture, "jpg", temp);
				} catch (Exception ex) {
				}
				String resultStr = TitanAgentCommonLib.sendPost(url + "/titanAgent/vmStat.htm", ht, temp);
				System.out.println(count + "\t" + sf.format(new Date()) + "\tversion=" + version);
				if (temp != null) {
					temp.delete();
				}
			}
			count++;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static String formatJSon(String str) {
		JsonParser parser = new JsonParser();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement el = parser.parse(str);
		return gson.toJson(el);
	}
}
